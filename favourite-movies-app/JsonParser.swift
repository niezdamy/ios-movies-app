//
//  JsonParser.swift
//  favourite-movies-app
//
//  Created by Oskar (Mac) on 16.07.2018.
//  Copyright © 2018 Oskar. All rights reserved.
//

import Foundation

class JsonParser {
    static func parse (data: Data) -> [String: AnyObject]? {
        let options = JSONSerialization.ReadingOptions()
        do{
            let json = try JSONSerialization.jsonObject(with: data, options: options) as? [String: AnyObject]
            return json!
        } catch (let parseError){
            print("There was an error parsing the JSON: \"\(parseError.localizedDescription)\"")
        }
        return nil
    }
}
